import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadSongs } from '../actions';
import AlbumsList from '../components/AlbumsList';

class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.props.loadSongs();
  }

  render() {
    const props = this.props;
    return (
      <AlbumsList { ...props }  />
    );
  }
}

function mapStateToProps(state) {
  return {
    sortedSongs: state.sortedSongs
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadSongs: () => dispatch(loadSongs())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListContainer);
