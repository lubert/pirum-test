import { SHOW_SONGS, SHOW_SONGS_ERROR } from '../config/constants';

const initialState = {
  sortedSongs: {}
};

export default (state = initialState, action) => {
  switch(action.type) {
    case SHOW_SONGS:
      return {
        ...state,
        sortedSongs: action.payload
      };

    case SHOW_SONGS_ERROR:
      return {
        ...state
      };

    default:
      return state;
  }
}
