import { SHOW_SONGS, SHOW_SONGS_ERROR } from '../config/constants';
import { songsSvc } from '../services';

export function loadSongs() {
  const req = songsSvc.getSortedSongs();
  return (dispatch) => {
    return req
      .then(response => {
        dispatch({
          type: SHOW_SONGS,
          payload: response
        });
      })
      .catch(err => {
        dispatch({
          type: SHOW_SONGS_ERROR,
          payload: err
        });
      })
  }
}
