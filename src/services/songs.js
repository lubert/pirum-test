import _ from 'lodash';
import connection from './connection';

const nest = (collection, keys) => {
  if(!keys.length) return collection;
  const [ first, ...rest ] = keys;
  return _.mapValues(_.groupBy(collection, first), value => nest(value, rest));
}

export class songsSvc {
  static getSongs() {
    return connection.request({
        method: 'GET',
        data: {}
      });
  }

  static getSortedSongs() {
    return connection.request({
        method: 'GET',
        data: {}
      })
      .then(response => {
        return nest(response, ['band', 'album']);
      })
      .catch(err => {
        return err;
      });
  }
};
