// import axios from 'axios';
import serverData from './data.json';

// Simulating server latency
const latency = (() => {
  const max = 20;
  const min = 5;

  return (Math.random() * (max - min) + min) * 100;
})();

class connection {
  static request({ method, data }) {
    // console.log(`Making a $method request with data:`, data);
    const exec = (resolve, reject) => {
      setTimeout(() => {
        // Axios request here
        // axios.get('./data.json')
        //   .then(resolve)

        resolve(serverData);
      }, latency);
    };

    return new Promise(exec);
  }
}

export default connection;
