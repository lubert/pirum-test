import React, { Component } from 'react';
import _ from 'lodash';

export default class Album extends Component {
  constructor(props) {
    super(props);
    this.toggleAlbum = this.toggleAlbum.bind(this);
    this.getIcon = this.getIcon.bind(this);

    this.state = {
      expanded: false
    };
  }

  toggleAlbum() {
    this.setState({ expanded: !this.state.expanded });
  }

  getIcon() {
    const icon = !this.state.expanded ? 'fa-plus text-danger' : 'fa-minus text-success';
    return(<i className={ 'fa ' + icon }  aria-hidden="true"></i>)
  }

  render() {
    const { name, songs } = this.props;
    return (
      <li className='list-group-item'>
        <h3>
          <a onClick={ this.toggleAlbum }>
            { this.getIcon() } <span>{ name }</span>
          </a>
        </h3>

        <ul className={ 'list-group ' + (this.state.expanded ? '' : 'hidden') }>
        {
          _.map(songs, (s, i) => (<li className='list-group-item' key={ i }> { (i + 1) + '. ' + s.song }</li>))
        }
        </ul>
      </li>
    );
  }
}
