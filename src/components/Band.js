import React, { Component } from 'react';
import Album from './Album';
import _ from 'lodash';

export default class Band extends Component {
  constructor(props) {
    super(props);
    this.toggleBand = this.toggleBand.bind(this);
    this.getIcon = this.getIcon.bind(this);
    this.state = {
      expanded: false
    };
  }

  toggleBand() {
    this.setState({ expanded: !this.state.expanded });
  }

  getIcon() {
    const icon = !this.state.expanded ? 'fa-plus text-danger' : 'fa-minus text-success';
    return(<i className={ 'fa ' + icon }  aria-hidden="true"></i>)
  }

  render() {
    const { name, albums } = this.props;
    return (
      <li className='list-group-item'>
        <h2>
          <a onClick={ this.toggleBand }>
            { this.getIcon() } <span>{ name }</span>
          </a>
        </h2>

        <ul className={ 'list-group ' + (this.state.expanded ? '' : 'hidden') }>
          {
            _.map(albums, (songs, name) =>
              (<Album key={ name } name={ name } songs={ songs } />))
          }
        </ul>
      </li>
    );
  }
}
