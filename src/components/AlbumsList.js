import React, { Component } from 'react';
import Band from './Band';
import _ from 'lodash';

export default class AlbumsList extends Component {
  render() {
    const { sortedSongs } = this.props;

    if(!sortedSongs) {
      return (
        <div>
          <i className="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i>
          <span className="sr-only">Loading...</span>
        </div>
      );
    }

    return (
      <div>
        <div className='page-header'>
          <h1>Pirum <small>Frontend developer test</small></h1>
        </div>
        <ul className='list-group'>
          {
            _.map(sortedSongs, (albums, name) => {
              return (<Band key={name} name={ name } albums={ albums } />);
            })
          }
        </ul>
      </div>
    );
  }
}
