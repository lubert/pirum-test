# Pirum Test

## What this test includes?

* React scaffolding for a web application
* React application using a Redux ecosystem
* Uses `redux-thunk` as a middleware providing the capability to run async actions
* Uses `redux-logger` for a fancy way to see the actions flow
* Division from Smart and Dump components making the app scalable
* Containers are bound to the store
* Mocks an API connection and it's ready to use `axios` (instead of jQuery) for async calls.
* Services are promise-based classes
* Adds a fake latency just to show a loader while the data is its way
* Uses a react-scripts to speed up the development process
* Uses Webpack as task runner
* Uses bootstrap + font awesome for the styling


## Making it work

First run the following commands on your preferred terminal
```
$ git clone git@bitbucket.org:lubert/pirum-test.git
$ npm install
$ npn start
```

After that you'll be automatically redirected to your browser showing the test solution. If not, please go manually to `http://localhost:3000/`
